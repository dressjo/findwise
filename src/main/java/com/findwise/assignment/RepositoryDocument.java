package com.findwise.assignment;

import java.util.List;

public interface RepositoryDocument {
    String getId();

    long getLength();

    List<String> getWords();
}
