package com.findwise.assignment;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Repository {

    public final static RepositoryDocument doc1 = new InMemoryDocument("1", "The brown fox jumped over the brown dog.");
    public final static RepositoryDocument doc2 = new InMemoryDocument("2", "The lazy brown dog, sat in the corner");
    public final static RepositoryDocument doc3 = new InMemoryDocument("3", "The Red Fox bit the lazy dog!");

    private static List<RepositoryDocument> documents = new ArrayList<>();

    static {
        documents.add(doc1);
        documents.add(doc2);
        documents.add(doc3);
    }

    public static List<RepositoryDocument> getDocuments() {
       return Collections.unmodifiableList(documents);
    }

}
