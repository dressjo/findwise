package com.findwise.assignment;

import java.math.BigDecimal;

public class SortedDocument {

    public String documentId;
    public BigDecimal normalizedTfIdfScore;

    public BigDecimal tfIdfScore;

    public SortedDocument(String documentId, BigDecimal normalizedTfIdfScore, BigDecimal tfIdfScore) {
        this.documentId = documentId;
        this.normalizedTfIdfScore = normalizedTfIdfScore;
        this.tfIdfScore = tfIdfScore;
    }

    public String getDocumentId() {
        return documentId;
    }

    public BigDecimal getNormalizedTfIdfScore() {
        return normalizedTfIdfScore;
    }

    public BigDecimal getTfIdfScore() {
        return tfIdfScore;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("SortedDocument{");
        sb.append("documentId='").append(documentId).append('\'');
        sb.append(", normalizedTfIdfScore=").append(normalizedTfIdfScore);
        sb.append(", tfIdfScore=").append(tfIdfScore);
        sb.append('}');
        return sb.toString();
    }

}