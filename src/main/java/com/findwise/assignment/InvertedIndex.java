package com.findwise.assignment;

import java.util.*;

/**
 *  Inverted index
 *
 *  Inverted index with search functionality
 */
public class InvertedIndex {

    private HashSet<String> corpus = new HashSet<>();
    private HashMap<String, HashMap<String, IndexDocument>> index = new HashMap<>(100);

    /**
     * Indexes a document
     *
     * @param document document to index
     */
    public void indexDocument(RepositoryDocument document) {
        final List<String> words = document.getWords();
        corpus.add(document.getId());
        for (String word : words) {
            HashMap<String, IndexDocument> documentMap = createWordEntry(word);
            indexDocument(document, documentMap, words.size());
        }
    }

    /**
     * Search for documents by single term
     *
     * @param term term to search for
     * @return list of documents sorted by length normalized tf-idf score
     */
    public List<SortedDocument> search(String term) {
        final HashMap<String, IndexDocument> wordIndexDocuments = index.get(term.toLowerCase());
        if (wordIndexDocuments == null) {
            return Collections.emptyList();
        }
        return new NormalizedTfIdfSorter(term, corpus.size(), new ArrayList<>(wordIndexDocuments.values())).sortDocuments();
    }

    private HashMap<String, IndexDocument> createWordEntry(String w) {
        HashMap<String, IndexDocument> documentMap = index.get(w);
        if (documentMap == null) {
            documentMap = new HashMap<String, IndexDocument>();
            index.put(w, documentMap);
        }
        return documentMap;
    }

    private void indexDocument(RepositoryDocument document, HashMap<String, IndexDocument> documentMap, int noTerms) {
        IndexDocument indexDocument = documentMap.get(document.getId());
        if (indexDocument == null) {
            indexDocument = new IndexDocument(document.getId(), noTerms, document.getLength());
            documentMap.put(document.getId(), indexDocument);
        }
        indexDocument.increase();
    }
}