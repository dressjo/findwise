package com.findwise.assignment;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class InMemoryDocument implements RepositoryDocument {
    private String id;
    private String document;
    private List<String> words = new ArrayList();

    static HashSet<String> stopWords = new HashSet<>();

    static {
        stopWords.add("the");
        stopWords.add("and");
        stopWords.add("in");
        stopWords.add("over");
    }

    public InMemoryDocument(String name, String document) {
        if (name == null)
            throw new IllegalArgumentException("Invalid value for name");
        if (document == null)
            throw new IllegalArgumentException("Invalid value for document");
        this.id = name;
        this.document = document;
        setWords(document);
    }

    private void setWords(String value) {
        final String[] base_words = value.split("\\W+");
        words = Stream.of(base_words).map(String::toLowerCase).filter(w -> !stopWords.contains(w)).collect(Collectors.toList());
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public long getLength() {
        return document.length();
    }

    @Override
    public List<String> getWords() {
        return Collections.unmodifiableList(words);
    }

    public String getDocument() {
        return document;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("InMemoryDocument{");
        sb.append("id='").append(id).append('\'');
        sb.append(", document='").append(document).append('\'');
        sb.append('}');
        return sb.toString();
    }

}
