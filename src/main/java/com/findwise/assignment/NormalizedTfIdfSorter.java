package com.findwise.assignment;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

public class NormalizedTfIdfSorter implements DocumentSorter {

    private List<IndexDocument> indexDocuments;
    private long corpusSize;
    private String term;

    public NormalizedTfIdfSorter(String term, long corpusSize, List<IndexDocument> indexDocuments) {
      this.corpusSize = corpusSize;
      this.indexDocuments = indexDocuments;
      this.term = term;
    }

    @Override
    public List<SortedDocument> sortDocuments() {
        final double averageDocumentLength = getAverageDocumentLength();
        List<SortedDocument> documentsToSort = new ArrayList();
        for (IndexDocument indexDocument : indexDocuments) {
            final BigDecimal tfIdfScore = getTfIdfScore(indexDocument);
            final BigDecimal normalizedTfIdfScore = TfIdf.getNormalizedTfIdfScore(tfIdfScore, indexDocument.getTermCount(), averageDocumentLength);
            documentsToSort.add(new SortedDocument(indexDocument.getId(), normalizedTfIdfScore, tfIdfScore));
        }

        List<SortedDocument> sortedDocuments = documentsToSort.stream()
                .sorted(Comparator.comparing(SortedDocument::getNormalizedTfIdfScore, Comparator.reverseOrder()))
                .collect(Collectors.toList());

        return sortedDocuments;
}

    protected BigDecimal getTfIdfScore(IndexDocument indexDocument) {
        final BigDecimal idf = TfIdf.getInverseDocumentFrequency(corpusSize, indexDocuments.size());
        final BigDecimal tf = TfIdf.getTermFrequency(indexDocument.getTermCount(), indexDocument.getLength());
        return TfIdf.getTfIdfScore(tf, idf);
    }

    private double getAverageDocumentLength() {
        LongSummaryStatistics stats = indexDocuments.stream()
                .collect(Collectors.summarizingLong(IndexDocument::getLength));
        return stats.getAverage();
    }

}
