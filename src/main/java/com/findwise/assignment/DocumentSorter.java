package com.findwise.assignment;

import java.util.List;

public interface DocumentSorter {
    List<SortedDocument> sortDocuments();
}
