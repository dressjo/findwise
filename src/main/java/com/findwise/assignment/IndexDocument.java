package com.findwise.assignment;

public class IndexDocument {

    private String id;
    private int termCount;
    private int totalTermCount;
    private long length;

    public IndexDocument(String name, int totalTermCount, long length) {
        this.id = name;
        this.totalTermCount = totalTermCount;
        this.length = length;
    }

    public long getLength() {
        return length;
    }

    public String getId() {
        return id;
    }

    public int getTermCount() {
        return termCount;
    }

    public void setTermCount(int termCount) {
        this.termCount = termCount;
    }

    public int getTotalTermCount() {
        return totalTermCount;
    }

    public void setTotalTermCount(int totalTermCount) {
        this.totalTermCount = totalTermCount;
    }

    public void increase() {
        termCount++;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("IndexDocument{");
        sb.append("id='").append(id).append('\'');
        sb.append(", termCount=").append(termCount);
        sb.append(", totalTermCount=").append(totalTermCount);
        sb.append('}');
        return sb.toString();
    }
}
