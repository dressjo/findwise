package com.findwise.assignment;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;

public class TfIdf {

    public static MathContext rounding = new MathContext(3, RoundingMode.HALF_UP);
    public static final double B_NORMALIZATION_FACTOR = 0.75d;

    /**
     * Calculate term frequency based with normalization on document length
     *
     * @param termCount
     * @param documentLength
     * @return
     */
    public static BigDecimal getTermFrequency(int termCount, long documentLength) {
        if(documentLength == 0)
            throw new ArithmeticException("Invalid documentLength");
        return new BigDecimal(termCount).divide(new BigDecimal(documentLength), rounding);
    }

    /**
     * Calculate inverse document frequency with log of base 10
     *
     * @param corpusSize
     * @param noDocumentsWithTerm
     * @return
     */
    public static BigDecimal getInverseDocumentFrequency(long corpusSize, long noDocumentsWithTerm) {
        return new BigDecimal(Math.log10(1 +((double)corpusSize)/(1 + noDocumentsWithTerm)) +1, rounding);
    }

    /**
     * Calculate tf-idf score
     *
     * @param tf
     * @param idf
     * @return
     */
    public static BigDecimal getTfIdfScore(BigDecimal tf, BigDecimal idf) {
       return tf.multiply(idf, rounding);
    }

    /**
     * Calculate normalized tf-idf score
     *
     * @param tfIdfScore
     * @param documentLength
     * @param averageDocumentLength
     * @return
     */
    public static BigDecimal getNormalizedTfIdfScore(BigDecimal tfIdfScore, int documentLength, double averageDocumentLength) {
        final double normalization = (double)1 / documentLength;
        final double norm = 1 / ((1d - B_NORMALIZATION_FACTOR) + B_NORMALIZATION_FACTOR * documentLength / averageDocumentLength);
        return tfIdfScore.multiply(new BigDecimal(norm), rounding);
    }

}