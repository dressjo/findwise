import com.findwise.assignment.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class Main {

    private static final Logger logger
            = LoggerFactory.getLogger(Main.class);

    private static InvertedIndex index = new InvertedIndex();

    static {
        for (RepositoryDocument repositoryDocument : Repository.getDocuments()) {
            index.indexDocument(repositoryDocument);
        }
    }

    public static void main(String[] args) {
        if (args.length == 0) {
            logger.info("No query term entered");
            return;
        }

        String queryTerm = args[0];
        printDocuments();
        printQueryTerm(queryTerm);
        final List<SortedDocument> result = index.search(queryTerm);
        if (result.isEmpty()) {
            logger.info("No results for query", queryTerm);
        } else {
            printResult(result);
        }
    }

    private static void printQueryTerm(String queryTerm) {
        logger.info("Query term: {}", queryTerm);
        logger.info("");
    }

    private static void printResult(List<SortedDocument> result) {
        logger.info("Result documents:");
        result.forEach(s -> logger.info(s.toString()));
    }

    private static void printDocuments() {
        logger.info("Repository Documents:");
        Repository.getDocuments().forEach(d -> logger.info(d.toString()));
        logger.info("");
    }

}
