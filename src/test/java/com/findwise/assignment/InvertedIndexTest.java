package com.findwise.assignment;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class InvertedIndexTest {

    private static final Logger logger
            = LoggerFactory.getLogger(InvertedIndexTest.class);

    private InvertedIndex invertedIndex;

    @BeforeEach
    void beforeEach() {
        invertedIndex = new InvertedIndex();
        for(RepositoryDocument document : Repository.getDocuments()) {
            invertedIndex.indexDocument(document);
        }
    }

    @Test
    void shouldReturnDocumentsForTerm_Brown() {
        final List<SortedDocument> result = invertedIndex.search("brown");
        logger.info("Brown");
        result.forEach(s -> logger.info(s.toString()));
        logger.info("");
    }

    @Test
    void shouldReturnDocumentsForTerm_Fox() {
        final List<SortedDocument> result = invertedIndex.search("fox");
        logger.info("Fox");
        result.forEach(s -> logger.info(s.toString()));
        logger.info("");
    }

    @Test
    void shouldReturnDocumentsForTerm_Dog() {
        final List<SortedDocument> result = invertedIndex.search("Dog");
        logger.info("Dog");
        result.forEach(s -> logger.info(s.toString()));
        logger.info("");
    }

}
