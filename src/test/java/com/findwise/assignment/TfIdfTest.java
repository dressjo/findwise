package com.findwise.assignment;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;

public class TfIdfTest {

    private static final Logger logger
            = LoggerFactory.getLogger(TfIdfTest.class);

    @Test
    void shouldCalculateTF() {
        final BigDecimal tf = TfIdf.getTermFrequency(3, 7);
        assertEquals(new BigDecimal("0.429"), tf);
    }

    @Test
    void shouldCalculateIDF() {
        final BigDecimal idf = TfIdf.getInverseDocumentFrequency(2,1);
        assertEquals(new BigDecimal("1.30"),idf);
    }

    @Test
    void shouldCalculateTFIDF() {
        final BigDecimal tf = TfIdf.getTermFrequency(3, 7);
        final BigDecimal idf = TfIdf.getInverseDocumentFrequency(2,1);
        final BigDecimal tf_idf = TfIdf.getTfIdfScore(tf,idf);
        assertEquals(new BigDecimal("0.558"),tf_idf);
    }

    @Test
    void shouldCalculateTFIDFWithNormalization() {
        final BigDecimal tf = TfIdf.getTermFrequency(3, 7);
        final BigDecimal idf = TfIdf.getInverseDocumentFrequency(2,1);
        final BigDecimal tf_idf = TfIdf.getTfIdfScore(tf,idf);
        final BigDecimal normalizedTfIdfScore = TfIdf.getNormalizedTfIdfScore(tf_idf,100, 50);
        assertEquals(new BigDecimal("0.319"), normalizedTfIdfScore);
    }


}
